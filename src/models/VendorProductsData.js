
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
var VendorProductSchema = new Schema({

            manufactureId: {
                type: String,
                required: true,
            },
            userId: {
                type: String,
                required: true,
            },
            productId: {
                type: String,
                required: true,
            },
            productName: {
                type: String,
                required: true,
            },
            productWeight: {
                type: String,
                required: true,
            },
            productPrice: {
                type: String,
                required: true,
            },
            pNumber: {
                type: Number,
                required: true,
            },
            imageUrl: {
                type: String,
                required: true,
            },
           
            
        },
           );
    var VendorProductsData = mongoose.model('VendorProductdata', VendorProductSchema);
     module.exports = VendorProductsData;

