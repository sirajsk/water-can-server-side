const express = require('express');
const router = express.Router();


const cartController =require('../../controllers/cart')

router.post('/addcart',cartController.addToCart)
router.put('/updateCart',cartController.updateCart)

router.get('/getquantity/:userId/:productId/:vendorId', (req, res) => {
    const userId = req.params.userId;
    const productId= req.params.productId;
    const vendorId = req.params.vendorId;
    cartController.getItemQuantityInCart(userId,productId,vendorId, res);
});
router.get('/cart/:id/:vendorId',(req,res)=>{
  const id = req.params.id;
  const vendorId = req.params.vendorId;
    cartController.getCartById(id,vendorId,res);
})
router.put('/remove/:id/:vendorId',(req,res)=>{
    const id = req.params.id;
    const vendorId = req.params.vendorId;
      cartController.removeCart(id,vendorId,res);})

module.exports = router;